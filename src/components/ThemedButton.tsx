import React from "react";

import { ThemeContext } from "../context/theme";

export const ThemedButton: React.FC = () => {
  const theme = React.useContext(ThemeContext);

  return (
    <div
      style={{
        display: "inline-flex",
        padding: "0.5em 1em",
        background: theme.code.background,
        color: theme.code.foreground
      }}
      onClick={theme.toggle}
    >
      ThemedButton
    </div>
  );
};
