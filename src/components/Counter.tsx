import React, { useReducer, useState } from "react";

import { counterInitialState, counterReducer } from "../reducer/counter";

export const Counter: React.FC = () => {
  const [inputValue, setInputValue] = useState("");
  const [counterState, counterDispatch] = useReducer(
    counterReducer,
    counterInitialState
  );

  const handleChangeText: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void = e => {
    setInputValue(e.target.value);
  };
  const handleClickIncrement: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void = () => {
    counterDispatch({ type: "decrement" });
  };
  const handleClickDecrement: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void = () => {
    counterDispatch({ type: "decrement" });
  };
  const handleClickAdd: (
    inputValue: string
  ) => (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void = inputValue => () => {
    const parsed = parseInt(inputValue, 10);
    if (isNaN(parsed)) {
      return;
    }
    counterDispatch({ type: "add", payload: { count: parsed } });
  };

  return (
    <div>
      <div>{counterState.count}</div>
      <button onClick={handleClickIncrement}>INCREMENT</button>
      <button onClick={handleClickDecrement}>DECREMENT</button>
      <input type="number" value={inputValue} onChange={handleChangeText} />
      <button onClick={handleClickAdd(inputValue)}>ADD</button>
    </div>
  );
};
