import React, { useState } from "react";

import { ThemeContext, themes } from "../context/theme";
import { Toolbar } from "./Toolbar";

export const ContextRoot: React.FC = () => {
  const [theme, setTheme] = useState(themes.dark);
  const toggleTheme = () => {
    theme === themes.light ? setTheme(themes.dark) : setTheme(themes.light);
  };

  return (
    <ThemeContext.Provider
      value={{
        code: theme,
        toggle: toggleTheme
      }}
    >
      <Toolbar />
      <button onClick={toggleTheme}>TOGGLE</button>
    </ThemeContext.Provider>
  );
};
