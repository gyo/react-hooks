import React from "react";

import { Counter } from "../components/Counter";

export const ReducerRoot: React.FC = () => {
  return (
    <>
      <Counter />
    </>
  );
};
