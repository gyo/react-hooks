import React from "react";

import { ThemedButton } from "./ThemedButton";

export const Toolbar: React.FC = () => {
  return (
    <ul
      style={{
        display: "flex",
        listStyle: "none",
        margin: 0,
        padding: "0.25em"
      }}
    >
      <li style={{ padding: "0.25em" }}>
        <ThemedButton />
      </li>
      <li style={{ padding: "0.25em" }}>
        <ThemedButton />
      </li>
    </ul>
  );
};
