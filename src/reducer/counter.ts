import { Reducer } from "react";

export type CounterState = {
  count: number;
};

export type CounterAction = {
  type: "increment" | "decrement" | "add";
  payload?: {
    count?: number;
  };
};

export const counterInitialState: CounterState = {
  count: 0
};

export const counterReducer: Reducer<CounterState, CounterAction> = (
  state,
  action
) => {
  if (action.type === "increment") {
    return { ...state, count: state.count + 1 };
  } else if (action.type === "decrement") {
    return { ...state, count: state.count + 1 };
  } else if (action.type === "add") {
    if (action.payload == null) {
      throw new Error("Payload is required.");
    }
    const count = action.payload.count;
    if (count == null) {
      throw new Error("Payload `count` is required.");
    }
    return { ...state, count: state.count + count };
  } else {
    throw new Error("Unexpected action type.");
  }
};
