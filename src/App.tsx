import "./App.css";

import React from "react";

import { ContextRoot } from "./components/ContextRoot";
import { ReducerRoot } from "./components/ReducerRoot";

const App: React.FC = () => {
  return (
    <>
      <h1>useContext</h1>
      <ContextRoot />
      <h1>useReducer</h1>
      <ReducerRoot />
    </>
  );
};

export default App;
