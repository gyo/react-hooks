import { createContext } from "react";

export type Theme = {
  foreground: string;
  background: string;
};

export type Themes = {
  light: Theme;
  dark: Theme;
};

export const themes: Themes = {
  light: {
    foreground: "#000",
    background: "#eee"
  },
  dark: {
    foreground: "#fff",
    background: "#222"
  }
};

export const ThemeContext = createContext({
  code: themes.dark,
  toggle: () => {}
});
